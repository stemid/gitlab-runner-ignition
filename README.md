# Gitlab runner ignition module

On CoreOS this module depends on [the serviceuser ignition module](https://gitlab.com/stemid/serviceuser-ignition.git) to enable a headless user to run quadlet services.

# Inputs

See [vars.tf](./vars.tf) file.

# Outputs

* ``config`` is an object meant to be used by other calling modules.
* ``json`` is plain text you can use to validate ignition or feed straight into a VM.
