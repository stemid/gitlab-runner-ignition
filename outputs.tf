output "config" {
  value = data.ignition_config.config
}

output "json" {
  value = data.ignition_config.config.rendered
  sensitive = true
}
