variable "runners" {
  type = list(object({
    name = string
    url = string
    token = string
    clone_url = optional(string)
    limit = optional(number, 4)
  }))
  description = "List of runners to create in config.toml"
}

variable "uid" {
  type = number
  description = "UID that will own all created files"
}

variable "gid" {
  type = number
  description = "GID that will own all created files"
}

variable "home" {
  type = string
  description = "Base dir for all created files"
}
