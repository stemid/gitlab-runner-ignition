# Temporary fix for issue: https://github.com/containers/podman/issues/3024#issuecomment-1722582668
data "ignition_directory" "user_override" {
  path = "/etc/systemd/system/user@.service.d"
}

data "ignition_file" "user_override" {
  path = "/etc/systemd/system/user@.service.d/override.conf"
  content {
    content = "[Service]\nOOMScoreAdjust="
  }
}

# Unfortunately disabling SElinux enforcing for gitlab runners is easiest
# because SElinux prevents containers from using socket files like docker.
data "ignition_file" "selinux" {
  path = "/etc/selinux/config"
  overwrite = true
  content {
    content = file("${path.module}/templates/selinux")
  }
}

# Enable podman socket for gitlab runner to use.
data "ignition_link" "podman_socket" {
  path = "${var.home}/.config/systemd/user/sockets.target.wants/podman.socket"
  target = "/usr/lib/systemd/user/podman.socket"
  hard = false
  uid = var.uid
  gid = var.gid
}

data "ignition_file" "runner_config" {
  path = "${var.home}/config.toml"
  content {
    #content = data.template_file.runner_config.rendered
    content = templatefile("${path.module}/templates/config.toml", {
      runners = var.runners
      uid = var.uid
    })
  }
  uid = var.uid
  gid = var.gid
}

data "ignition_file" "runner_container_unit" {
  path = "${var.home}/.config/containers/systemd/gitlab-runner.container"
  content {
    content = templatefile("${path.module}/templates/gitlab-runner.container", {
      uid = var.uid
    })
  }
  uid = var.uid
  gid = var.gid
}

data "ignition_config" "config" {
  links = [
    data.ignition_link.podman_socket.rendered,
  ]

  files = [
    data.ignition_file.selinux.rendered,
    data.ignition_file.runner_config.rendered,
    data.ignition_file.runner_container_unit.rendered,
    data.ignition_file.user_override.rendered,
  ]

  directories = [
    data.ignition_directory.user_override.rendered,
  ]
}
